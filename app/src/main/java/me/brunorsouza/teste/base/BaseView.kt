package me.brunorsouza.teste.base

interface BaseView {

    fun showLoading()

    fun dismissLoading()

    fun showError(show: Boolean)

}