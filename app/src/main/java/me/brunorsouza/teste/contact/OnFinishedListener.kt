package me.brunorsouza.teste.contact

import me.brunorsouza.teste.webservice.CellResponse

interface OnFinishedListener {

    fun onSuccess(cellResponse: CellResponse)

    fun onError(errorMessage: String?)

}