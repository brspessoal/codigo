package me.brunorsouza.teste.contact.view

import me.brunorsouza.teste.base.BaseView
import me.brunorsouza.teste.contact.ContactLayout
import me.brunorsouza.teste.webservice.CellResponse

/**
 * Created by Bruno Souza.
 */
interface ContactView : BaseView {

    fun showSuccessView()

    fun dismissSuccessView()

    fun onError(errorMessage: String?)

    fun getContactLayout(): ContactLayout

    fun clear()

    fun isContactError(): Boolean

    fun showError()

    fun dismissError()

    fun setListeners()

}