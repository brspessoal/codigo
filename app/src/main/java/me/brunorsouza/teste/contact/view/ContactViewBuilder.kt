package me.brunorsouza.teste.contact.view

import me.brunorsouza.teste.base.Type
import me.brunorsouza.teste.contact.Cell
import me.brunorsouza.teste.contact.ContactLayout
import me.brunorsouza.teste.contact.FieldCell
import me.brunorsouza.teste.contact.view.custom.CheckboxCellView
import me.brunorsouza.teste.contact.view.custom.SendCellView
import me.brunorsouza.teste.contact.view.custom.TextCellView
import me.brunorsouza.teste.contact.view.custom.TextFieldCellView

class ContactViewBuilder(private val rootView: ContactLayout) {

    fun proccessCellsFormView(cells: MutableList<Cell>): MutableList<CellView>{
        val listCellView = mutableListOf<CellView>()

        cells.forEach { cell ->
            listCellView.add(getCellView(cell))
        }

        rootView.setCellsView(listCellView)

        return listCellView
    }

    private fun getCellView(cell: Cell): CellView {
        return when (cell.type) {
            Type.field.position -> TextFieldCellView(cell as FieldCell, rootView)
            Type.text.position -> TextCellView(cell, rootView)
            Type.checkbox.position -> CheckboxCellView(cell, rootView)
            Type.send.position -> SendCellView(cell, rootView)
            else -> TextCellView(cell, rootView)
        }
    }
}