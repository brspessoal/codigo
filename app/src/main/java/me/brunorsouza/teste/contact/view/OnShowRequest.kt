package me.brunorsouza.teste.contact.view

interface OnShowRequest {

    fun showView(id: Int)

    fun dismissView(id: Int)

}