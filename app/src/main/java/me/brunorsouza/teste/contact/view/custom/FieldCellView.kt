package me.brunorsouza.teste.contact.view.custom

import android.view.ViewGroup
import me.brunorsouza.teste.contact.FieldCell
import me.brunorsouza.teste.contact.view.CellView

abstract class FieldCellView(fieldCell: FieldCell,
                             rootView: ViewGroup): CellView(fieldCell, rootView){

    abstract fun isValid(): Boolean

    abstract fun showError()

    abstract fun dismissError()

    abstract fun clear()

}