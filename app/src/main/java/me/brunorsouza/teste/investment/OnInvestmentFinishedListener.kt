package me.brunorsouza.teste.investment

import me.brunorsouza.teste.webservice.InvestmentResponseFormated

interface OnInvestmentFinishedListener {

    fun onSuccess(investmentResponse: InvestmentResponseFormated)

    fun onError(errorMessage: String?)

}