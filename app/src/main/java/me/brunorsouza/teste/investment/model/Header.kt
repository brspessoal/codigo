package me.brunorsouza.teste.investment.model

data class Header (val title: String,
                  val assetName: String,
                  val whatIs: String,
                  val definition: String)