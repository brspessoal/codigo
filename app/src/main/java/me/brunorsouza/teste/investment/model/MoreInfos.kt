package me.brunorsouza.teste.investment.model

data class MoreInfos (val title: String,
                     val onMonth: Info,
                     val onYear: Info,
                     val onTwelveMonths: Info)