package me.brunorsouza.teste.investment.model

data class Risk (val title: String,
                val risk: Int)