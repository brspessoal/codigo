package me.brunorsouza.teste.investment.model.mapper

import me.brunorsouza.teste.investment.model.Info
import me.brunorsouza.teste.webservice.Screen

object DownInfosMapper {

    fun map(screen: Screen): MutableList<Info> {
        val listInfos = mutableListOf<Info>()

        screen.downInfo.mapTo(listInfos, { info -> Info(info.name, info.data ?: "") })

        return listInfos
    }

}