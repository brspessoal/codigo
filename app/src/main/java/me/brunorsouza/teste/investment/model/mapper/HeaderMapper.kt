package me.brunorsouza.teste.investment.model.mapper

import me.brunorsouza.teste.investment.model.Header
import me.brunorsouza.teste.webservice.Screen

object HeaderMapper {

    fun map(screen: Screen) = Header(
            screen.title,
            screen.fundName,
            screen.whatIs,
            screen.definition
    )

}