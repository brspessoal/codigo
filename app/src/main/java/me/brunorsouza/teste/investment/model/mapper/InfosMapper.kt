package me.brunorsouza.teste.investment.model.mapper

import me.brunorsouza.teste.investment.model.Info
import me.brunorsouza.teste.webservice.Screen

object InfosMapper {

    fun map(screen: Screen): MutableList<Info> {
        val listInfos = mutableListOf<Info>()

        screen.info.mapTo(listInfos, { info -> Info(info.name, info.data ?: "")})

        return listInfos
    }

}