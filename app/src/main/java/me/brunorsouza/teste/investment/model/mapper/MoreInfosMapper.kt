package me.brunorsouza.teste.investment.model.mapper

import me.brunorsouza.teste.investment.model.Info
import me.brunorsouza.teste.investment.model.MoreInfos
import me.brunorsouza.teste.webservice.Screen

object MoreInfosMapper {

    fun map(screen: Screen) = MoreInfos(
            screen.infoTitle,
            Info(screen.monthInfo.fund.toString(), screen.monthInfo.cdi.toString()),
            Info(screen.yearInfo.fund.toString(), screen.yearInfo.cdi.toString()),
            Info(screen.twelveInfo.fund.toString(), screen.twelveInfo.cdi.toString())
    )

}