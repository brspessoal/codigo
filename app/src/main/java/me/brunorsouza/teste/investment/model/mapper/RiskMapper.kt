package me.brunorsouza.teste.investment.model.mapper

import me.brunorsouza.teste.investment.model.Risk
import me.brunorsouza.teste.webservice.Screen

object RiskMapper {

    fun map(screen: Screen) = Risk (
            screen.riskTitle,
            screen.risk
    )

}