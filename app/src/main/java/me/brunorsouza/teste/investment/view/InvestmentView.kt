package me.brunorsouza.teste.investment.view

import me.brunorsouza.teste.base.BaseView
import me.brunorsouza.teste.investment.model.Header
import me.brunorsouza.teste.investment.model.Info
import me.brunorsouza.teste.investment.model.MoreInfos
import me.brunorsouza.teste.investment.model.Risk

interface InvestmentView : BaseView {

    fun setHeader(headerInfo: Header)

    fun setRisk(riskInfo: Risk)

    fun setMoreInfos(moreInfos: MoreInfos)

    fun setListInfos(infos: MutableList<Info>)

    fun setDownInfos(infos: MutableList<Info>)

    fun onError(txt: String?)

}