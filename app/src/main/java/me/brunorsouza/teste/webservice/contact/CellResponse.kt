package me.brunorsouza.teste.webservice

import me.brunorsouza.teste.contact.Cell

/**
 * Created by brunosouza.
 */
data class CellResponse(
        val cells: MutableList<Cell>
)